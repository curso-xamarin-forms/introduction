﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {
        public Home()
        {
            InitializeComponent();
            this.label1.Text = "Hola desde ContentPage";
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            bool result = await DisplayAlert("Cambiar label", "Desea cambiar el texto", "Aceptar", "Cancelar");
            if (result)
            {
                //this.label2.Text = "Label 2 cambiado";
                this.label2.Text = this.entry1.Text;
            }
            string color = await DisplayActionSheet("Cambiar color", "Cancelar", null, "Azul", "Verde", "Amarillo");
            if (!string.IsNullOrEmpty(color))
            {
                if (color != "Cancelar")
                {
                    switch (color)
                    {
                        case "Azul":
                            this.label2.TextColor = Color.Blue;
                            break;
                        case "Verde":
                            this.label2.TextColor = Color.Green;
                            break;
                        case "Amarillo":
                            this.label2.TextColor = Color.Yellow;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            Page2 page2 = new Page2();
            page2.name.Text = this.entry1.Text;
            await Navigation.PushAsync(page2);
        }

        private async void Button_Clicked_3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }

        private async void Button_Clicked_4(object sender, EventArgs e)
        {
            string pagina = await DisplayActionSheet("Navegar a ...", "Cancelar", null, "Page 2", "Page 3", "Page 4","PersonPage");
            if (!string.IsNullOrEmpty(pagina))
            {
                if (pagina != "Cancelar")
                {
                    switch (pagina)
                    {
                        case "Page 2":
                            await Navigation.PushAsync(new Page2());
                            break;
                        case "Page 3":
                            await Navigation.PushAsync(new Page3());
                            break;
                        case "Page 4":
                            await Navigation.PushAsync(new Page4());
                            break;
                        case "PersonPage":
                            await Navigation.PushAsync(new PersonPage());
                            break;
                        default:
                            break;
                    }
                }
            }
            
        }
    }
}