﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {

        #region Variables

        #endregion

        #region Constructores
        public Page4()
        {
            InitializeComponent();
            this.initPicker();
        }
        #endregion

        #region Métodos
        private void initPicker()
        {
            var lista = new List<string>();
            lista.Add("Manabi");
            lista.Add("Los Rios");
            lista.Add("Zamora Chinchipe");
            lista.Add("Napo");
            lista.Add("Guayas");

            this.picker.ItemsSource = lista;
            this.listProvincia.ItemsSource = lista;
        } 
        #endregion

        #region Eventos de controles
        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.provincia.Text = this.picker.SelectedItem.ToString();
        }

        private void ListProvincia_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            this.provincia.Text = this.listProvincia.SelectedItem.ToString();
        }
        #endregion

        #region Eventos de página
        protected override void OnAppearing()
        {
            Debug.WriteLine("Page4 OnAppearing");
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            Debug.WriteLine("Page4 OnDisappearing");
            base.OnDisappearing();
        }

        // Hardware Back Button (Android and Windows)
        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }
        #endregion


    }
}