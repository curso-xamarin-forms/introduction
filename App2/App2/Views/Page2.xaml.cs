﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2(string name)
        {
            InitializeComponent();
            this.name.Text = name;
        }

        public Page2()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            //await Navigation.PopAsync();
            this.btn1.IsEnabled = false;
            this.indicator.IsVisible = true;
            this.indicator.IsRunning = true;

            await this.name.RotateTo(360, 1000);

            this.btn1.IsEnabled = true;
            this.indicator.IsVisible = false;
            this.indicator.IsRunning = false;
        }

        private void Activate_Toggled(object sender, ToggledEventArgs e)
        {
            if (this.activate.IsToggled)
            {
                this.description.IsVisible = true;
            }
            else
            {
                this.description.IsVisible = false;
            }
        }

        private void Dob_DateSelected(object sender, DateChangedEventArgs e)
        {
            this.name.Text = this.dob.Date.ToShortDateString();
        }
    }
}