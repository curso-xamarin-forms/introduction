﻿using App2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersonPage : ContentPage
    {
        Persona persona { get; set; }

        public PersonPage()
        {
            persona = new Persona();
            this.BindingContext = persona;

            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            string mensaje = "Nombres: " + this.persona.Nombres
                + "\nApellidos: " + this.persona.Apellidos
                + "\nFecha Nacimiento: " + this.persona.DOB.ToShortDateString();

            await DisplayAlert("Datos de la Persona", mensaje, "OK");
        }
    }
}