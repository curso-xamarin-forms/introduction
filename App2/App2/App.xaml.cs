﻿using App2.Views;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace App2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Home());
        }

        protected override void OnStart()
        {
            Debug.WriteLine("App OnStart");
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("App OnSleep");
        }

        protected override void OnResume()
        {
            Debug.WriteLine("App OnResume");
        }
    }
}
