﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace App2.Models
{
    public class Persona : INotifyPropertyChanged
    {
        #region Atributos

        public string nombres { get; set; }
        public string apellidos { get; set; }
        public DateTime dob { get; set; }

        #endregion Atributos

        #region Propiedades

        public string Nombres
        {
            get
            {
                return nombres;
            }
            set
            {
                nombres = value;
                OnPropertyChanged();
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }
            set
            {
                apellidos = value;
                OnPropertyChanged();
            }
        }

        public DateTime DOB
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
                OnPropertyChanged();
            }
        }

        #endregion Propiedades

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this,
            new PropertyChangedEventArgs(propertyName));
        }
    }
}